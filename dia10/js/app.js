/* 
Esta funcion es basica e importante, ya que espera que el browser este listo
para comenzar a ejecutar el Javascript.

Sin esta funcion puede ser que muchas librerias no funcionen correctamente.

$(document).ready(function() {

  AQUI SE COLOCARA LA CONFIGURACION

})
*/

$(document).ready(function() {
  
  /*configuracion de la libreria lightbox*/
  lightbox.option({
    'resizeDuration': 1000,
    'wrapAround': true
  })
  
  /* configuracion de la libreria slidesjs */
  $("#slides").slidesjs({
    width: 940,
    height: 528
  });
  
  $("a.btn").on('click', function () {
    var element = $(this);
    element.toggleClass('mensaje')
  });
});